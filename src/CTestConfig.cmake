find_program(COVERAGE_COMMAND "gcov")
find_program(MEMORYCHECK_COMMAND "valgrind")

set(MEMORYCHECK_COMMAND_OPTIONS
    "--trace-children=yes --leak-check=full --gen-suppressions=all")

set(COVERAGE_EXTRA_FLAGS "-l -b -a -c")

set(CTEST_PROJECT_NAME "SafeLogger")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")
set(CTEST_DROP_METHOD "https")
set(CTEST_DROP_SITE "my.cdash.org")
set(CTEST_DROP_LOCATION "/submit.php?project=SafeLogger")
set(CTEST_DROP_SITE_CDASH TRUE)
