#ifndef SAFE_LOGGER_CONSOLE_APPENDER_FACTORY_H
#define SAFE_LOGGER_CONSOLE_APPENDER_FACTORY_H

#include "safe-logger-appender/appender.hpp"

extern "C" safelogger::Appender * createAppender(void);

#endif