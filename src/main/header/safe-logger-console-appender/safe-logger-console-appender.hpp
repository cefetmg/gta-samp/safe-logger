#ifndef SAFE_LOGGER_CONSOLE_APPENDER_H
#define SAFE_LOGGER_CONSOLE_APPENDER_H

#include "safe-logger-appender/appender.hpp"

namespace safelogger {

class SafeLoggerConsoleAppender : public Appender {

    public:

        ~SafeLoggerConsoleAppender() override;

        void appendStandardMessage(const std::string message) override;

        void appendErrorMessage(const std::string errorMessage) override;

};

}

#endif