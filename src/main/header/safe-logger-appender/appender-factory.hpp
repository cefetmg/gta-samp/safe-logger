#ifndef SAFE_LOGGER_APPENDER_FACTORY_H
#define SAFE_LOGGER_APPENDER_FACTORY_H

#include "safe-logger-appender/appender.hpp"

typedef safelogger::Appender * createAppender_t (void);

#endif