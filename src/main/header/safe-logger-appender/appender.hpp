#ifndef SAFE_LOGGER_APPENDER_H
#define SAFE_LOGGER_APPENDER_H

#include <string>

namespace safelogger {

class Appender {

    public:

        virtual ~Appender() {};

        virtual void appendStandardMessage(const std::string message) = 0;

        virtual void appendErrorMessage(const std::string errorMessage) = 0;

};

}



#endif