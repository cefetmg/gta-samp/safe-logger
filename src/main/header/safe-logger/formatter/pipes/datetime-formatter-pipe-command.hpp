#ifndef SAFE_LOGGER_DATETIME_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_DATETIME_FORMATTER_PIPE_COMMAND_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"

namespace safelogger {

class DateTimeFormatterPipeCommand : public FormatterPipeCommand {

    public:
        static const size_t LOG_DATE_TIME_MAX_SIZE;

    private:

        static const std::string LOG_DATE_TIME_LABEL;

        static const std::string LOG_MILLIS_LABEL;

        const std::string m_dateTimeFormat;

    public:

        DateTimeFormatterPipeCommand(const std::string t_dateTimeFormat);

        ~DateTimeFormatterPipeCommand() override;

        const std::string runFormat(const std::string &t_messageFormat) const override;

};

}

#endif