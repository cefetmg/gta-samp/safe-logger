#ifndef SAFE_LOGGER_LOGGER_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_LOGGER_FORMATTER_PIPE_COMMAND_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include <string>

namespace safelogger {

class LoggerFormatterPipeCommand : public FormatterPipeCommand {

    private:

        static const std::string LOG_NAME_LABEL;

        const std::string m_loggerName;

    public:

        LoggerFormatterPipeCommand(const std::string t_loggerName);

        ~LoggerFormatterPipeCommand();

        const std::string runFormat(const std::string &t_messageFormat) const override;

};

}

#endif