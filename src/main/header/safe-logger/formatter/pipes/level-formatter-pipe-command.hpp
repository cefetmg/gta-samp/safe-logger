#ifndef SAFE_LOGGER_LEVEL_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_LEVEL_FORMATTER_PIPE_COMMAND_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include "safe-logger/log-level.hpp"
#include <string>

namespace safelogger {

class LevelFormatterPipeCommand : public FormatterPipeCommand {

    private:

        static const std::string LOG_LEVEL_LABEL;

        const LogLevel m_logLevel;

    public:

        LevelFormatterPipeCommand(const LogLevel &t_logLevel);

        ~LevelFormatterPipeCommand();

        const std::string runFormat(const std::string &t_messageFormat) const override;

};

}

#endif