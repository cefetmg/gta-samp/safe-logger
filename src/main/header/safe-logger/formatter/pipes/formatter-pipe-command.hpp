#ifndef SAFE_LOGGER_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_FORMATTER_PIPE_COMMAND_H

#include <string>

namespace safelogger {

class FormatterPipeCommand {

    public:

        virtual ~FormatterPipeCommand() {};

        virtual const std::string runFormat(const std::string &messageFormat) const = 0;

    protected:
    
        static const std::string replaceAll(const std::string source, const std::string pattern, const std::string target);

};

}

#endif