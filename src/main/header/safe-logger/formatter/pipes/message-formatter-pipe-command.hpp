#ifndef SAFE_LOGGER_MESSAGE_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_MESSAGE_FORMATTER_PIPE_COMMAND_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include <vector>
#include <string>

namespace safelogger {

class MessageFormatterPipeCommand : public FormatterPipeCommand {

    private:

        static const std::string ARGUMENT_PATTERN;

        static const std::string LOG_MESSAGE_LABEL;

        const std::string m_logMessage;

        std::vector<std::string> m_arguments;

    public:

        MessageFormatterPipeCommand(const std::string &t_logMessage, std::vector<std::string> t_arguments);

        ~MessageFormatterPipeCommand() override;

        const std::string runFormat(const std::string &t_messageFormat) const override;

};

}

#endif