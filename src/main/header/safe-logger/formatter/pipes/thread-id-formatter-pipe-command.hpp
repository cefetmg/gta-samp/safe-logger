#ifndef SAFE_LOGGER_THREAD_ID_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_THREAD_ID_FORMATTER_PIPE_COMMAND_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"

namespace safelogger {

class ThreadIdFormatterPipeCommand : public FormatterPipeCommand {

    private:
        static const std::string LOG_THREAD_ID_LABEL;

    public:

        ThreadIdFormatterPipeCommand();

        ~ThreadIdFormatterPipeCommand() override;

        const std::string runFormat(const std::string &t_messageFormat) const override;
};

}

#endif