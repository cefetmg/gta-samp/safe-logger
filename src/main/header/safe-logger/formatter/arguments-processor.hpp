#ifndef SAFE_LOGGER_ARG_PROCESSOR_H
#define SAFE_LOGGER_ARG_PROCESSOR_H

#include <string>
#include <vector>
#include <sstream>

namespace safelogger {

    class ArgumentsProcessor {

        private:

            static const char FS = 0x1C;

            inline bool isEndOfArgument(const std::string &argument) const {
                return argument.size() == 1 && argument[0] == FS;
            }

            std::string uniformizeMultilineString(std::vector<std::string> vectorOfStrings) const {
                std::string result = "";
                if (!vectorOfStrings.empty()) {
                    size_t lineIdx = 0;
                    result += vectorOfStrings[lineIdx];
                    for (lineIdx = 1; lineIdx < vectorOfStrings.size(); lineIdx++) {
                        result += ("\n" + vectorOfStrings[lineIdx]);  
                    }
                }
                return result;
            }

        public:

            template <typename ... Args>
            std::vector<std::string> process(const Args &... arguments) const {
                std::stringstream stream;
                int sink[] = { 0, ((void)(stream << arguments << std::endl << FS << std::endl), 0)... };
                (void) sink;
                std::vector<std::string> result;
                std::vector<std::string> argumentComposite;
                std::string argument;

                while (getline(stream, argument)) {
                    if (isEndOfArgument(argument)) {
                        result.push_back(uniformizeMultilineString(argumentComposite));
                        argument.clear();
                        argumentComposite.clear();
                    } else {
                        argumentComposite.push_back(argument);
                    }
                }

                return result;
            }
        
    };
}

#endif