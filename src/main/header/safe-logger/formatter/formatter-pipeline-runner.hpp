#ifndef SAFE_LOGGER_FORMATTER_PIPELINE_FACTORY_H
#define SAFE_LOGGER_FORMATTER_PIPELINE_FACTORY_H

#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include <vector>
#include <string>

namespace safelogger {

class FormatterPipelineRunner {

    private:

        std::vector<FormatterPipeCommand *> m_commandList;

    public:

        FormatterPipelineRunner();

        FormatterPipelineRunner & add(FormatterPipeCommand *t_formatter);

        const std::string run(const std::string &t_messageFormat);

};

}

#endif