#ifndef SAFE_LOGGER_STANDARD_FORMATTER_PIPELINE_RUNNER_FACTORY_H
#define SAFE_LOGGER_STANDARD_FORMATTER_PIPELINE_RUNNER_FACTORY_H

#include "safe-logger/formatter/formatter-pipeline-runner-facade.hpp"
#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"

namespace safelogger {

class StandardFormatterPipelineRunnerFacade : public FormatterPipelineRunnerFacade {

    public:

        StandardFormatterPipelineRunnerFacade();

        ~StandardFormatterPipelineRunnerFacade() override;

        const std::string createFrom(
            const std::string t_dateTimeFormat, 
            const LogLevel &t_logLevel, 
            const std::string &t_loggerName,
            const std::string &t_logMessage, 
            const std::vector<std::string> &t_messageArguments,
            const std::string &t_messageFormat) override;

};

}

#endif