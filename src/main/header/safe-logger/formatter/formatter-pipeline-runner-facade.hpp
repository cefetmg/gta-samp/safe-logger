#ifndef SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_FACTORY_H
#define SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_FACTORY_H

#include "safe-logger/log-level.hpp"
#include <string>
#include <vector>

namespace safelogger {

class FormatterPipelineRunnerFacade {

    public:

        virtual ~FormatterPipelineRunnerFacade() {}

        virtual const std::string createFrom(const std::string t_dateTimeFormat, 
            const LogLevel &t_logLevel, 
            const std::string &t_loggerName,
            const std::string &t_logMessage, 
            const std::vector<std::string> &t_messageArguments,
            const std::string &t_messageFormat) = 0;

};

}

#endif