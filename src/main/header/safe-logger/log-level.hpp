#ifndef SAFE_LOGGER_LOG_LEVEL_H
#define SAFE_LOGGER_LOG_LEVEL_H

#include <iostream>
#include <unordered_map>
#include <string>

namespace safelogger {
    class LogLevel;
}

std::ostream & operator << (std::ostream &out, const safelogger::LogLevel &logLevel);

namespace safelogger {

class LogLevel {

    protected:

        unsigned int m_level;

        const char *m_label;

        LogLevel(unsigned int t_level, const char *t_label);

    public:

        static const LogLevel OFF;

        static const LogLevel FATAL;

        static const LogLevel ERROR;
        
        static const LogLevel WARN;
        
        static const LogLevel INFO;
      
        static const LogLevel DEBUG;
        
        static const LogLevel TRACE;

        static const LogLevel ALL;
        
    public:

        unsigned int getLevel() const;

        const char* getLabel() const;

        bool operator == (const LogLevel &t_other) const;

        bool operator != (const LogLevel &t_other) const;

        bool operator < (const LogLevel &t_other) const;

        bool operator > (const LogLevel &t_other) const;

        bool operator <= (const LogLevel &t_other) const;

        bool operator >= (const LogLevel &t_other) const;
};

}

#endif