file(GLOB SAFE_LOGGER_LOG_LEVEL_SRC
     ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger/log-level.hpp
     ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger/log-level.cpp)

file(GLOB SAFE_LOGGER_MESSAGE_FORMATTER_PIPES_SRC
     ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger/formatter/pipes/*.hpp
     ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger/formatter/pipes/*.cpp)

file(
  GLOB
  SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_SRC
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger/formatter/formatter-pipeline-runner.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger/formatter/formatter-pipeline-runner.cpp
)

file(
  GLOB
  SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_FACADE_SRC
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger/formatter/formatter-pipeline-runner-facade.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger/formatter/standard-formatter-pipeline-runner-facade.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger/formatter/standard-formatter-pipeline-runner-facade.cpp
)

file(
  GLOB SAFE_LOGGER_APPENDER_SRC
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger-appender/appender.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger-appender/appender-factory.hpp)

file(
  GLOB
  SAFE_LOGGER_SRC
  ${SAFE_LOGGER_LOG_LEVEL_SRC}
  ${SAFE_LOGGER_ARGUMENTS_PROCESSOR_SRC}
  ${SAFE_LOGGER_MESSAGE_FORMATTER_PIPES_SRC}
  ${SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_SRC}
  ${SAFE_LOGGER_FORMATTER_PIPELINE_RUNNER_FACADE_SRC}
  ${SAFE_LOGGER_APPENDER_SRC})

set_property(GLOBAL PROPERTY SAFE_LOGGER_SRC ${SAFE_LOGGER_SRC})

find_package(OpenMP REQUIRED)

add_library(safe_logger STATIC ${SAFE_LOGGER_SRC})

target_include_directories(safe_logger PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/header
                                              ${CMAKE_CURRENT_SOURCE_DIR}/impl)

target_link_libraries(safe_logger PUBLIC OpenMP::OpenMP_CXX)

file(
  GLOB
  SAFE_LOGGER_CONSOLE_APPENDER_SRC
  ${SAFE_LOGGER_APPENDER_SRC}
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger-console-appender/safe-logger-console-appender.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger-console-appender/safe-logger-console-appender.cpp
)

file(
  GLOB
  SAFE_LOGGER_CONSOLE_APPENDER_EXPORTS_SRC
  ${SAFE_LOGGER_CONSOLE_APPENDER_SRC}
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger-console-appender/safe-logger-console-appender-factory.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/header/safe-logger-console-appender/module.def
  ${CMAKE_CURRENT_SOURCE_DIR}/impl/safe-logger-console-appender/safe-logger-console-appender-factory.cpp
)

set_property(GLOBAL PROPERTY SAFE_LOGGER_CONSOLE_APPENDER_SRC
                             ${SAFE_LOGGER_CONSOLE_APPENDER_SRC})

add_library(safe_logger_console_appender SHARED
            ${SAFE_LOGGER_CONSOLE_APPENDER_EXPORTS_SRC})

target_include_directories(
  safe_logger_console_appender PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/header
                                      ${CMAKE_CURRENT_SOURCE_DIR}/impl)

target_link_libraries(safe_logger_console_appender PUBLIC OpenMP::OpenMP_CXX)
