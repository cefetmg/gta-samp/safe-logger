#include "safe-logger/formatter/standard-formatter-pipeline-runner-facade.hpp"
#include "safe-logger/formatter/formatter-pipeline-runner.hpp"
#include "safe-logger/formatter/pipes/datetime-formatter-pipe-command.hpp"
#include "safe-logger/formatter/pipes/level-formatter-pipe-command.hpp"
#include "safe-logger/formatter/pipes/logger-formatter-pipe-command.hpp"
#include "safe-logger/formatter/pipes/message-formatter-pipe-command.hpp"
#include "safe-logger/formatter/pipes/thread-id-formatter-pipe-command.hpp"

using namespace std;
using namespace safelogger;

vector<FormatterPipeCommand *> getCommands(
    const string t_dateTimeFormat, 
    const LogLevel &t_logLevel, 
    const string &t_loggerName,
    const string &t_logMessage, 
    const vector<string> &t_messageArguments) {

    return { 
        new DateTimeFormatterPipeCommand(t_dateTimeFormat),
        new LevelFormatterPipeCommand(t_logLevel),
        new LoggerFormatterPipeCommand(t_loggerName),
        new MessageFormatterPipeCommand(t_logMessage, t_messageArguments),
        new ThreadIdFormatterPipeCommand()
    };
}

void clearCommands(vector<FormatterPipeCommand *> &commands) {
    while (!commands.empty()) {
        FormatterPipeCommand *command = commands.back();
        commands.pop_back();
        delete command;
    }
}

StandardFormatterPipelineRunnerFacade::StandardFormatterPipelineRunnerFacade() {}

StandardFormatterPipelineRunnerFacade::~StandardFormatterPipelineRunnerFacade() {}

const string StandardFormatterPipelineRunnerFacade::createFrom(
    const string t_dateTimeFormat, 
    const LogLevel &t_logLevel, 
    const string &t_loggerName,
    const string &t_logMessage, 
    const vector<string> &t_messageArguments,
    const string &t_messageFormat) {

    FormatterPipelineRunner runner = FormatterPipelineRunner();
    vector<FormatterPipeCommand *> commands = getCommands(t_dateTimeFormat, t_logLevel, t_loggerName, t_logMessage, t_messageArguments);

    for (FormatterPipeCommand *command : commands) {
        runner = runner.add(command);
    }

    string result = runner.run(t_messageFormat);
   
    clearCommands(commands);

    return result;
}