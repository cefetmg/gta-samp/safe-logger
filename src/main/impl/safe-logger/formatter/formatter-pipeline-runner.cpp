#include "safe-logger/formatter/formatter-pipeline-runner.hpp"

using namespace std;
using namespace safelogger;

FormatterPipelineRunner::FormatterPipelineRunner(): 
    m_commandList(vector<FormatterPipeCommand *>()) {}

FormatterPipelineRunner & FormatterPipelineRunner::add(FormatterPipeCommand *t_formatter) {
    m_commandList.push_back(t_formatter);
    return *this;
}


const string FormatterPipelineRunner::run(const string &t_messageFormat) {
    string result = string(t_messageFormat);
    for (FormatterPipeCommand *formatter : m_commandList) {
        result = formatter->runFormat(result);
    }
    m_commandList.clear();
    return result;
}
