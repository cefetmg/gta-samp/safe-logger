#include "safe-logger/formatter/pipes/logger-formatter-pipe-command.hpp"

using namespace safelogger;
using namespace std;

const string LoggerFormatterPipeCommand::LOG_NAME_LABEL = "%logger";

LoggerFormatterPipeCommand::LoggerFormatterPipeCommand(const string t_loggerName):
    m_loggerName(t_loggerName) {}

LoggerFormatterPipeCommand::~LoggerFormatterPipeCommand() {}

const string LoggerFormatterPipeCommand::runFormat(const string &t_messageFormat) const {
    return FormatterPipeCommand::replaceAll(t_messageFormat, LoggerFormatterPipeCommand::LOG_NAME_LABEL, m_loggerName);
}