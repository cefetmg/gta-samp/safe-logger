#include "safe-logger/formatter/pipes/level-formatter-pipe-command.hpp"

using namespace std;
using namespace safelogger;

const string LevelFormatterPipeCommand::LOG_LEVEL_LABEL = "%level";

LevelFormatterPipeCommand::LevelFormatterPipeCommand(const LogLevel &t_logLevel): 
    m_logLevel(t_logLevel) {}

LevelFormatterPipeCommand::~LevelFormatterPipeCommand() {}

const string LevelFormatterPipeCommand::runFormat(const string &t_messageFormat) const {
    return FormatterPipeCommand::replaceAll(t_messageFormat, LevelFormatterPipeCommand::LOG_LEVEL_LABEL, m_logLevel.getLabel());
}