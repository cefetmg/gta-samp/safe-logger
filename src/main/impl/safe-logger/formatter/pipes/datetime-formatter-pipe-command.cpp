#include "safe-logger/formatter/pipes/datetime-formatter-pipe-command.hpp"
#include <time.h>
#include <sys/time.h>

using namespace std;
using namespace safelogger;

const string DateTimeFormatterPipeCommand::LOG_DATE_TIME_LABEL = "%datetime";

const string DateTimeFormatterPipeCommand::LOG_MILLIS_LABEL = "{millis}";

const size_t DateTimeFormatterPipeCommand::LOG_DATE_TIME_MAX_SIZE = 256;

DateTimeFormatterPipeCommand::DateTimeFormatterPipeCommand(const string t_dateTimeFormat): m_dateTimeFormat(t_dateTimeFormat) {}

DateTimeFormatterPipeCommand::~DateTimeFormatterPipeCommand() {}

const string DateTimeFormatterPipeCommand::runFormat(const string &t_messageFormat) const {
    char formattedTime[DateTimeFormatterPipeCommand::LOG_DATE_TIME_MAX_SIZE];
    char millis[4];
    timeval timeStructure;
    tm *timeInfo;
    gettimeofday(&timeStructure, nullptr);
    timeInfo = localtime(&timeStructure.tv_sec);
    
    sprintf(millis, "%03ld", timeStructure.tv_usec/1000);

    strftime(formattedTime, DateTimeFormatterPipeCommand::LOG_DATE_TIME_MAX_SIZE, m_dateTimeFormat.c_str(), timeInfo);
    
    string formattedDateTimeAsString = FormatterPipeCommand::replaceAll(string(formattedTime), DateTimeFormatterPipeCommand::LOG_MILLIS_LABEL, millis);
    return FormatterPipeCommand::replaceAll(t_messageFormat, DateTimeFormatterPipeCommand::LOG_DATE_TIME_LABEL, formattedDateTimeAsString);
}