#include "safe-logger/formatter/pipes/message-formatter-pipe-command.hpp"

using namespace std;
using namespace safelogger;

const string MessageFormatterPipeCommand::ARGUMENT_PATTERN = "{}";

const string MessageFormatterPipeCommand::LOG_MESSAGE_LABEL = "%message";

MessageFormatterPipeCommand::MessageFormatterPipeCommand(const std::string &t_logMessage, vector<string> t_arguments): 
    m_logMessage(t_logMessage), 
    m_arguments(t_arguments) { }

MessageFormatterPipeCommand::~MessageFormatterPipeCommand() { }

const std::string MessageFormatterPipeCommand::runFormat(const std::string &t_messageFormat) const {
    string logMessageCopy = string(m_logMessage);

    size_t position = logMessageCopy.find(MessageFormatterPipeCommand::ARGUMENT_PATTERN);
    size_t argumentIndex = 0;
    while (position != string::npos && argumentIndex < m_arguments.size()) {
        logMessageCopy.replace(position, MessageFormatterPipeCommand::ARGUMENT_PATTERN.size(), m_arguments[argumentIndex]);
        position = logMessageCopy.find(MessageFormatterPipeCommand::ARGUMENT_PATTERN, position + m_arguments[argumentIndex].size());
        argumentIndex++;
    }

    return FormatterPipeCommand::replaceAll(t_messageFormat, MessageFormatterPipeCommand::LOG_MESSAGE_LABEL, logMessageCopy);
}