#include "safe-logger/formatter/pipes/thread-id-formatter-pipe-command.hpp"
#include <omp.h>

using namespace safelogger;
using namespace std;

const std::string ThreadIdFormatterPipeCommand::LOG_THREAD_ID_LABEL = "%thread";

ThreadIdFormatterPipeCommand::ThreadIdFormatterPipeCommand() { }

ThreadIdFormatterPipeCommand::~ThreadIdFormatterPipeCommand() { }

const std::string ThreadIdFormatterPipeCommand::runFormat(const std::string &t_messageFormat) const {
    return FormatterPipeCommand::replaceAll(t_messageFormat, ThreadIdFormatterPipeCommand::LOG_THREAD_ID_LABEL, to_string(omp_get_thread_num()));
}