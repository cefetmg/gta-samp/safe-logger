#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include <string>

using namespace safelogger;
using namespace std;

const string FormatterPipeCommand::replaceAll(const string source, const string pattern, string target) {
    string result = string(source);
    size_t position = result.find(pattern);
    while (position != string::npos) {
        result.replace(position, pattern.size(), target);
        position = result.find(pattern, position + target.size());
    }
    return result;
}