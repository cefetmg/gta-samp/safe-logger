#include "safe-logger/log-level.hpp"
#include <limits.h>
#include <string.h>
#include <omp.h>

using namespace std;
using namespace safelogger;

const LogLevel LogLevel::OFF = LogLevel(0, "");

const LogLevel LogLevel::FATAL = LogLevel(1, "FATAL");

const LogLevel LogLevel::ERROR = LogLevel(2, "ERROR");

const LogLevel LogLevel::WARN = LogLevel(3, "WARN");

const LogLevel LogLevel::INFO = LogLevel(4, "INFO");

const LogLevel LogLevel::DEBUG = LogLevel(5, "DEBUG");

const LogLevel LogLevel::TRACE = LogLevel(6, "TRACE");

const LogLevel LogLevel::ALL = LogLevel(UINT_MAX, "ALL");

LogLevel::LogLevel(unsigned int t_level, const char *t_label) : m_level(t_level), m_label(t_label) { }

unsigned int LogLevel::getLevel() const { return m_level; }

const char* LogLevel::getLabel() const { return m_label; }

bool LogLevel::operator == (const LogLevel &t_other) const { return getLevel() == t_other.getLevel(); }

bool LogLevel::operator != (const LogLevel &t_other) const { return !(*this == t_other); }

bool LogLevel::operator < (const LogLevel &t_other) const { return getLevel() < t_other.getLevel(); }

bool LogLevel::operator > (const LogLevel &t_other) const { return getLevel() > t_other.getLevel(); }

bool LogLevel::operator <= (const LogLevel &t_other) const { return getLevel() <= t_other.getLevel(); }

bool LogLevel::operator >= (const LogLevel &t_other) const { return getLevel() >= t_other.getLevel(); }

ostream & operator << (ostream &out, const LogLevel &t_other) {
    out << t_other.getLabel();
    return out;
}