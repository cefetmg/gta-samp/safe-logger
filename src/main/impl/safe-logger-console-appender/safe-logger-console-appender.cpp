#include "safe-logger-console-appender/safe-logger-console-appender.hpp"
#include <iostream>

using namespace std;
using namespace safelogger;

SafeLoggerConsoleAppender::~SafeLoggerConsoleAppender() { }

void SafeLoggerConsoleAppender::appendStandardMessage(const string message) {
    cout << message << '\n';
}

void SafeLoggerConsoleAppender::appendErrorMessage(const string errorMessage) {
    cerr << errorMessage << '\n';
}