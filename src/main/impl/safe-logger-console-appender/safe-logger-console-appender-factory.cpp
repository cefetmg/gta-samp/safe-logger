#include "safe-logger-console-appender/safe-logger-console-appender-factory.hpp"
#include "safe-logger-console-appender/safe-logger-console-appender.hpp"

extern "C" safelogger::Appender * createAppender(void) {
    return new safelogger::SafeLoggerConsoleAppender();
}