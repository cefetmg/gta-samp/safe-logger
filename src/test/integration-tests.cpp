
#include "gtest/gtest.h"
#include <string>
#include <stdio.h>
#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif

static std::string CURRENT_PATH;

#include "safe-logger-console-appender/safe-logger-console-appender-integration-test.hpp"

int main(int argc, char *argv[]) {
    std::string execDir = argv[0];
    CURRENT_PATH = execDir.substr(0, execDir.find_last_of("/")).c_str();

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}