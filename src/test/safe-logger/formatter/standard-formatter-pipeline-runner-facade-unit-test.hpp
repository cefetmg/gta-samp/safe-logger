#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "safe-logger/formatter/standard-formatter-pipeline-runner-facade.hpp"
#include "safe-logger/formatter/formatter-pipeline-runner.hpp"

using namespace std;
using namespace safelogger;

class StandardFormatterPipelineRunnerFacadeUnitTest : public ::testing::Test {

    protected:

        FormatterPipelineRunnerFacade *facade;

        string result;

    protected:

        StandardFormatterPipelineRunnerFacadeUnitTest() {}

        ~StandardFormatterPipelineRunnerFacadeUnitTest() {}

        void SetUp() override {
            facade = new StandardFormatterPipelineRunnerFacade();
        }

        void TearDown() override {
            delete facade;
            result.clear();
        }

};

TEST_F(StandardFormatterPipelineRunnerFacadeUnitTest, ShouldFormatMessageGivenInputParameters) {
    result = facade->createFrom(
        "%Y-%m-%d %T.{millis}", 
        LogLevel::INFO, 
        "MyClass", 
        "This is a test of a message with a single parameter: {}.", 
        {"Dummy\nparam"}, 
        "[%datetime] %logger t-%thread - %level - %message");
    
    ASSERT_THAT(result, ::testing::MatchesRegex("\\[[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]\\.[0-9][0-9][0-9]\\]\\ (.)*"));
    ASSERT_THAT(result, ::testing::HasSubstr("MyClass t-0 - INFO - This is a test of a message with a single parameter: Dummy\nparam."));
}

TEST_F(StandardFormatterPipelineRunnerFacadeUnitTest, ShouldOutputJsonFormat) {
    result = facade->createFrom(
        "%Y-%m-%d %T.{millis}%z", 
        LogLevel::FATAL, 
        "MyClass",
        "Mega Man X {} > Mega Man X {} > Mega Man X {}", 
        {"1", "2", "Command Mission"}, 
        "{ \"dateTime\": \"%datetime\", \"className\": \"%logger\", \"level\": \"%level\", \"thread\": %thread, \"message\": \"%message\" }");
    
    ASSERT_THAT(result, ::testing::MatchesRegex("(.)*[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]\\.[0-9][0-9][0-9][\\+\\-][0-9][0-9][0-9][0-9](.)*"));
    ASSERT_THAT(result, ::testing::HasSubstr("\"className\": \"MyClass\","));
    ASSERT_THAT(result, ::testing::HasSubstr("\"level\": \"FATAL\","));
    ASSERT_THAT(result, ::testing::HasSubstr("\"thread\": 0,"));
    ASSERT_THAT(result, ::testing::HasSubstr("\"message\": \"Mega Man X 1 > Mega Man X 2 > Mega Man X Command Mission\""));
}

TEST_F(StandardFormatterPipelineRunnerFacadeUnitTest, ShouldIgnoreFieldsNotDeclaredInFormat) {
    result = facade->createFrom(
        "%Y-%m-%d %T.{millis}", 
        LogLevel::INFO, 
        "MyClass", 
        "This is a test of a message with a single parameter: {}.", 
        {"Dummy\nparam"}, 
        "%logger t%thread - %message");
    
    ASSERT_THAT(result, ::testing::Eq("MyClass t0 - This is a test of a message with a single parameter: Dummy\nparam."));
}