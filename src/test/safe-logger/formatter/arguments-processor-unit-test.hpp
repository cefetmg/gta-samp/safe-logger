#include "gtest/gtest.h"
#include "safe-logger/log-level.hpp"
#include "safe-logger/formatter/arguments-processor.hpp"
#include <string>
#include <vector>
#include <stdexcept>

using namespace safelogger;
using namespace std;

class ArugmentsProcessorUnitTest : public ::testing::Test {

    protected:

        ArgumentsProcessor argumentsProcessor;

    protected:

        ArugmentsProcessorUnitTest() {}

        ~ArugmentsProcessorUnitTest() {}

        void SetUp() override {}

        void TearDown() override {}

};

TEST_F(ArugmentsProcessorUnitTest, ShouldConvertListOfNumbersToListOfStrings) {
    vector<string> result = argumentsProcessor.process(1, 2, 1000, 4, 5, 100, 909, -11, 3.1416);

    ASSERT_EQ(9, result.size());
    ASSERT_EQ("1", result[0]);
    ASSERT_EQ("2", result[1]);
    ASSERT_EQ("1000", result[2]);
    ASSERT_EQ("4", result[3]);
    ASSERT_EQ("5", result[4]);
    ASSERT_EQ("100", result[5]);
    ASSERT_EQ("909", result[6]);
    ASSERT_EQ("-11", result[7]);
    ASSERT_EQ("3.1416", result[8]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldConvertListOfCharsAndNumbersToListOfStrings) {
    vector<string> result = argumentsProcessor.process("the", "Brown", "fox", 30, "jumps", 450.11);

    ASSERT_EQ(6, result.size());
    ASSERT_EQ("the", result[0]);
    ASSERT_EQ("Brown", result[1]);
    ASSERT_EQ("fox", result[2]);
    ASSERT_EQ("30", result[3]);
    ASSERT_EQ("jumps", result[4]);
    ASSERT_EQ("450.11", result[5]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldConvertLogLevelsAndMessagesToListOfStrings) {
    vector<string> result = argumentsProcessor.process("the", LogLevel::FATAL, "brown", LogLevel::OFF, 30, LogLevel::ALL, "fox");

    ASSERT_EQ(7, result.size());
    ASSERT_EQ("the", result[0]);
    ASSERT_EQ("FATAL", result[1]);
    ASSERT_EQ("brown", result[2]);
    ASSERT_EQ("", result[3]);
    ASSERT_EQ("30", result[4]);
    ASSERT_EQ("ALL", result[5]);
    ASSERT_EQ("fox", result[6]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldNotConvertEmptyListOfArgs) {
    vector<string> result = argumentsProcessor.process();

    ASSERT_TRUE(result.empty());
}

TEST_F(ArugmentsProcessorUnitTest, ShouldReadSingleStringGivenMultiSpacedString) {
    vector<string> result = argumentsProcessor.process("how do I read multispace strings?", "another string");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("how do I read multispace strings?", result[0]);
    ASSERT_EQ("another string", result[1]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldReadSingleStringGivenMultiLineString) {
    vector<string> result = argumentsProcessor.process("how do I read\nmulti-line strings?", "another string");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("how do I read\nmulti-line strings?", result[0]);
    ASSERT_EQ("another string", result[1]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldReadSingleStringGivenMultiLineStringWithEmptyTextsBetween) {
    vector<string> result = argumentsProcessor.process("how do I read\nmulti-line\n\n\tstrings?", "another\t string");

    ASSERT_EQ(2, result.size());
    ASSERT_EQ("how do I read\nmulti-line\n\n\tstrings?", result[0]);
    ASSERT_EQ("another\t string", result[1]);
}

TEST_F(ArugmentsProcessorUnitTest, ShouldReadExceptionMessageBetweenDifferentArguments) {
    vector<string> result = argumentsProcessor.process("next is an exception", invalid_argument("exception try").what(), "next is\njust PI", 3.1416, "here ends");

    ASSERT_EQ(5, result.size());
    ASSERT_EQ("next is an exception", result[0]);
    ASSERT_EQ("exception try", result[1]);
    ASSERT_EQ("next is\njust PI", result[2]);
    ASSERT_EQ("3.1416", result[3]);
    ASSERT_EQ("here ends", result[4]);
}