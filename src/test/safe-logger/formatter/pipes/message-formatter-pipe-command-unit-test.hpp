#ifndef SAFE_LOGGER_MESSAGE_FORMATTER_PIPE_COMMAND_UNIT_TEST_H
#define SAFE_LOGGER_MESSAGE_FORMATTER_PIPE_COMMAND_UNIT_TEST_H

#include "gtest/gtest.h"
#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include "safe-logger/formatter/pipes/message-formatter-pipe-command.hpp"
#include <vector>
#include <string>

using namespace std;
using namespace safelogger;

class MessageFormatterPipeCommandUnitTest : public ::testing::Test {

    protected:

        FormatterPipeCommand *pipeCommand;

        std::string result;

    protected:

        MessageFormatterPipeCommandUnitTest() {}

        ~MessageFormatterPipeCommandUnitTest() {}

        void SetUp() override {
            pipeCommand = nullptr;
        }

        void TearDown() override {
            if (pipeCommand != nullptr) {
                delete pipeCommand;
            }
            result.clear();
        }
};

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldNotFormatLogGivenEmptyLogMessage) {
    pipeCommand = new MessageFormatterPipeCommand("", vector<string>());

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldNotFormatLogGivenLogMessagePatternNotPresent) {
    pipeCommand = new MessageFormatterPipeCommand("This message should not appear", vector<string>());

    result = pipeCommand->runFormat("This message should appear");

    ASSERT_EQ(result, "This message should appear");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogGivenLogMessagePatternPresent) {
    pipeCommand = new MessageFormatterPipeCommand("This message should also appear", vector<string>());

    result = pipeCommand->runFormat("This message should appear - %message");

    ASSERT_EQ(result, "This message should appear - This message should also appear");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogIncludingArgumentsVector) {
    pipeCommand = new MessageFormatterPipeCommand("{} {} of {}", vector<string>({"The", "Legend", "Zelda"}));

    result = pipeCommand->runFormat("Best game of the world: %message");

    ASSERT_EQ(result, "Best game of the world: The Legend of Zelda");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogIncludingArgumentsVectorWithEmptyString) {
    pipeCommand = new MessageFormatterPipeCommand("{} {} of {}{}", vector<string>({"The", "Legend", "Zelda", ""}));

    result = pipeCommand->runFormat("Best game of the world: %message");

    ASSERT_EQ(result, "Best game of the world: The Legend of Zelda");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogInlcudingArgumentsWithNumbers) {
    pipeCommand = new MessageFormatterPipeCommand("{}/{} elements processed", vector<string>({"3", "5"}));

    result = pipeCommand->runFormat("[Logging parameters]: %message");

    ASSERT_EQ(result, "[Logging parameters]: 3/5 elements processed");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogKeepingKeysWhoseArgumentsSizeDoesNotMatch) {
    pipeCommand = new MessageFormatterPipeCommand("I ate four fruits: {}, {}, {} and {}.", vector<string>({"pineapple", "pen"}));

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "I ate four fruits: pineapple, pen, {} and {}.");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogDiscardingExceedingArguments) {
    pipeCommand = new MessageFormatterPipeCommand("I ate two fruits: {} and {}.", vector<string>({"apple", "pineapple", "pen"}));

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "I ate two fruits: apple and pineapple.");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldFormatLogReplacingMessageMoreThanOnceIfRequired) {
    pipeCommand = new MessageFormatterPipeCommand("\"Hello world, {}.\"", vector<string>({"Drigo"}));

    result = pipeCommand->runFormat("First occurence: %message. Second occurence: %message. Extra text here.");

    ASSERT_EQ(result, "First occurence: \"Hello world, Drigo.\". Second occurence: \"Hello world, Drigo.\". Extra text here.");
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldReturnCopyOfInputParameter) {
    string input = "I ate two fruits: {} and {}.";
    pipeCommand = new MessageFormatterPipeCommand(input, vector<string>({"apple", "pineapple", "pen"}));

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "I ate two fruits: apple and pineapple.");
    ASSERT_NE(&input, &result);
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldReturnMessageWithLineBreaks) {
    string input = "I ate two\nfruits:\n\t- {}; and\n\t- {}.";
    pipeCommand = new MessageFormatterPipeCommand(input, vector<string>({"apple pineapple", "pen"}));

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "I ate two\nfruits:\n\t- apple pineapple; and\n\t- pen.");
    ASSERT_NE(&input, &result);
}

TEST_F(MessageFormatterPipeCommandUnitTest, ShouldReplaceFormatWithStringAndLineBreaks) {
    string input = "I ate two\nfruits:{}- {}; and{}- {}.";
    pipeCommand = new MessageFormatterPipeCommand(input, vector<string>({"\n\t", "apple pineapple", "\n\t", "pen"}));

    result = pipeCommand->runFormat("%message");

    ASSERT_EQ(result, "I ate two\nfruits:\n\t- apple pineapple; and\n\t- pen.");
    ASSERT_NE(&input, &result);
}

#endif