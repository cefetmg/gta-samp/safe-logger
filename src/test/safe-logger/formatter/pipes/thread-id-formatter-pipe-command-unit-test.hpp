#include "gtest/gtest.h"
#include "safe-logger/formatter/pipes/thread-id-formatter-pipe-command.hpp"
#include <omp.h>
#include <set>

using namespace std;
using namespace safelogger;

class ThreadIdFormatterPipeCommandUnitTest : public ::testing::Test {

    protected:

        set<string> results;

        FormatterPipeCommand *formatter;

    protected:

        ThreadIdFormatterPipeCommandUnitTest() { }

        ~ThreadIdFormatterPipeCommandUnitTest() {}

        void SetUp() override {
            formatter = nullptr;
        }

        void TearDown() override {
            if (formatter != nullptr) {
                delete formatter;
            }
            results.clear();
        }

};

TEST_F(ThreadIdFormatterPipeCommandUnitTest, ShouldReturnSingleThreadFormat) {
    formatter = new ThreadIdFormatterPipeCommand();
    
    results.insert(formatter->runFormat("Simple message with a thread %thread id"));

    ASSERT_NE(results.end(), results.find("Simple message with a thread 0 id"));
}

TEST_F(ThreadIdFormatterPipeCommandUnitTest, ShouldFormatMessagesWithTwoThreads) {
    formatter = new ThreadIdFormatterPipeCommand();
    string result;

    #pragma omp parallel private(result) shared(results) num_threads(2)
    {

        result = formatter->runFormat("Simple message with a thread %thread id");
        #pragma omp critical        
            results.insert(result);
    }

    ASSERT_NE(results.end(), results.find("Simple message with a thread 0 id"));
    ASSERT_NE(results.end(), results.find("Simple message with a thread 1 id"));
}

TEST_F(ThreadIdFormatterPipeCommandUnitTest, ShouldHaveSeveralFormatsAndProcessThemInParallel) {
    formatter = new ThreadIdFormatterPipeCommand();
    string result;

    #pragma omp parallel private(result) shared(results) num_threads(4)
    {
        result = formatter->runFormat("Simple %message with a thread %thread id");
        #pragma omp critical
            results.insert(result);
    }

    ASSERT_NE(results.end(), results.find("Simple %message with a thread 0 id"));
    ASSERT_NE(results.end(), results.find("Simple %message with a thread 1 id"));
    ASSERT_NE(results.end(), results.find("Simple %message with a thread 2 id"));
    ASSERT_NE(results.end(), results.find("Simple %message with a thread 3 id"));
}