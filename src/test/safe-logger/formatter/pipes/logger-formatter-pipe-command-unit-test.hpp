#include "gtest/gtest.h"
#include "safe-logger/formatter/pipes/logger-formatter-pipe-command.hpp"

using namespace std;
using namespace safelogger;

class LoggerFormatterPipeCommandUnitTest : public ::testing::Test {

    protected:

        string result;

        FormatterPipeCommand *formatter;

    protected:

        LoggerFormatterPipeCommandUnitTest() {}

        ~LoggerFormatterPipeCommandUnitTest() {}

        void SetUp() override { 
            formatter = nullptr;
        }

        void TearDown() override {
            if (formatter != nullptr) {
                delete formatter;
            }
            result.clear();
        }
};

TEST_F(LoggerFormatterPipeCommandUnitTest, ShouldAddClassNameToLoggerMessage) {
    formatter = new LoggerFormatterPipeCommand("MyClass");

    result = formatter->runFormat("%datetime %level - this is a test = %logger: dummy info.");

    ASSERT_EQ(result, "%datetime %level - this is a test = MyClass: dummy info.");
}

TEST_F(LoggerFormatterPipeCommandUnitTest, ShouldNotAddClassNameToLoggerMessage) {
    formatter = new LoggerFormatterPipeCommand("MyClass");

    result = formatter->runFormat("%datetime %level - this is a test = : dummy info.");

    ASSERT_EQ(result, "%datetime %level - this is a test = : dummy info.");
}