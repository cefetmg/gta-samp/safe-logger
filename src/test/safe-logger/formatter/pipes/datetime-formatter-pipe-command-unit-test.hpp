#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "safe-logger/formatter/pipes/datetime-formatter-pipe-command.hpp"
#include <string>

using namespace std;
using namespace safelogger;

class DateTimeFormatterPipeCommandUnitTest : public ::testing::Test {

    protected:

        FormatterPipeCommand *formatter;

        string result;

    protected:
        DateTimeFormatterPipeCommandUnitTest() {}

        ~DateTimeFormatterPipeCommandUnitTest() {}

        void SetUp() override {

        }

        void TearDown() override {
            if (formatter != nullptr) {
                delete formatter;
            }
            result.clear();
        }

};

TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldNotFormatDateTimeGivenLabelNotPresent) {
    formatter = new DateTimeFormatterPipeCommand("wont-be-used-anyways");

    result = formatter->runFormat("should not replace date time since label does not exist");

    ASSERT_EQ(result, "should not replace date time since label does not exist");
}

TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldNotFormatDateTimeGivenLogFormatNotPresent) {
    formatter = new DateTimeFormatterPipeCommand("wont-be-used-anyways");

    result = formatter->runFormat("");

    ASSERT_TRUE(result.empty());
}

TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldNotFormatDateTimeGivenFormatStringNotValid) {
    formatter = new DateTimeFormatterPipeCommand("wont-produce-results");

    result = formatter->runFormat("I should replace this -> %datetime <- ");

    ASSERT_EQ(result, "I should replace this -> wont-produce-results <- ");
}

TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldOutputFormattedDateTimeWithoutMilliseconds) {
    formatter = new DateTimeFormatterPipeCommand("%Y-%m-%d %T");

    result = formatter->runFormat("%datetime");

    ASSERT_THAT(result, ::testing::MatchesRegex("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]"));
}
 
TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldOutputFormattedDateTimeWithMilliseconds) {
    formatter = new DateTimeFormatterPipeCommand("%Y-%m-%d %T.{millis}");

    result = formatter->runFormat("%datetime");

    ASSERT_THAT(result, ::testing::MatchesRegex("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]\\.[0-9][0-9][0-9]"));
}

TEST_F(DateTimeFormatterPipeCommandUnitTest, ShouldOutputFormattedDateTimeWithMillisecondsTwice) {
    formatter = new DateTimeFormatterPipeCommand("%Y-%m-%d %T.{millis}");

    result = formatter->runFormat("%datetime %datetime");

    ASSERT_THAT(result, ::testing::MatchesRegex("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]\\.[0-9][0-9][0-9]\\ [0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]\\.[0-9][0-9][0-9]"));
}