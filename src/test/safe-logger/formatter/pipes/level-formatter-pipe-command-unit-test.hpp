#include "gtest/gtest.h"
#include "safe-logger/formatter/pipes/level-formatter-pipe-command.hpp"
#include <string>

using namespace std;
using namespace safelogger;

class LevelFormatterPipeCommandUnitTest : public ::testing::Test {

    protected:

        FormatterPipeCommand *formatter;

        string result;

    protected:

        LevelFormatterPipeCommandUnitTest() {}

        ~LevelFormatterPipeCommandUnitTest() {}

        void SetUp() override {
            formatter = nullptr;
        }

        void TearDown() override {
            if (formatter != nullptr) {
                delete formatter;
            }
            result.clear();
        }

};

TEST_F(LevelFormatterPipeCommandUnitTest, ShouldAddLevelToMessage) {
    formatter = new LevelFormatterPipeCommand(LogLevel::DEBUG);

    result = formatter->runFormat("This is just a %message -> %level -> %message");

    ASSERT_EQ(result, "This is just a %message -> DEBUG -> %message");
}

TEST_F(LevelFormatterPipeCommandUnitTest, ShouldNotAddLevelToMessage) {
    formatter = new LevelFormatterPipeCommand(LogLevel::DEBUG);

    result = formatter->runFormat("This is just a %message -> -> %message");

    ASSERT_EQ(result, "This is just a %message -> -> %message");
}