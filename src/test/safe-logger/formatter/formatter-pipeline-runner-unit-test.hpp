#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "safe-logger/formatter/formatter-pipeline-runner.hpp"
#include "safe-logger/mock/mock-formatter-pipe-command.hpp"

using namespace std;
using namespace safelogger;

class FormatterPiepelineRunnerUnitTest : public ::testing::Test {

    protected:

        MockFormatterPipeCommand formatter;

        FormatterPipelineRunner runner;

        string result;

    protected:

        FormatterPiepelineRunnerUnitTest() {}

        ~FormatterPiepelineRunnerUnitTest() {}

        void SetUp() override {
            runner = FormatterPipelineRunner();
        }

        void TearDown() override { 
            result.clear();

        }

};

TEST_F(FormatterPiepelineRunnerUnitTest, ShouldProduceSameMemoryAddressWhenAddingFormatter) {
    EXPECT_CALL(formatter, runFormat(::testing::_))
        .Times(0);

    runner = runner.add(&formatter)
        .add(&formatter)
        .add(&formatter);

    ASSERT_EQ(&runner, &runner);   
}

TEST_F(FormatterPiepelineRunnerUnitTest, ShouldFormatMessageGivenFullPipelineIncluded) {
    EXPECT_CALL(formatter, runFormat(::testing::Eq("second")))
        .WillOnce(::testing::Return("third"));
    EXPECT_CALL(formatter, runFormat(::testing::Eq("first")))
        .WillOnce(::testing::Return("second"));
    EXPECT_CALL(formatter, runFormat(::testing::Eq("dummy message")))
        .WillOnce(::testing::Return("first"));

    result = runner.add(&formatter)
        .add(&formatter)
        .add(&formatter)
        .run("dummy message");

    ASSERT_EQ(result, "third");   
}

TEST_F(FormatterPiepelineRunnerUnitTest, ShouldCallDifferentFormattersGivenPipeCommand) {
    MockFormatterPipeCommand otherFormatter;
    EXPECT_CALL(formatter, runFormat(::testing::Eq("dummy message")))
        .WillOnce(::testing::Return("dummy message for first formatter"));
    EXPECT_CALL(otherFormatter, runFormat((::testing::Eq("dummy message for first formatter"))))
        .WillOnce(::testing::Return("dummy message final"));

    result = runner.add(&formatter)
        .add(&otherFormatter)
        .run("dummy message");

    ASSERT_EQ(result, "dummy message final");   
}