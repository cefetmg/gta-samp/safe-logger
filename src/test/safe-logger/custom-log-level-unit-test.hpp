#include "safe-logger/log-level.hpp"
#include "gtest/gtest.h"
#include <sstream>

using namespace std;
using namespace safelogger;

class CustomLogLevel : public LogLevel {  
    private:
        CustomLogLevel(): LogLevel(1000, "CUSTOM_LEVEL") {}

    public:
        static const LogLevel CUSTOM;

};

const LogLevel CustomLogLevel::CUSTOM = CustomLogLevel();

class ShouldBeSameAsAllLevel : public LogLevel {
    private:
        ShouldBeSameAsAllLevel(): LogLevel(-1, "ANOTHER_ALL") {}

    public:
        static const LogLevel SAME_AS_ALL;
};

const LogLevel ShouldBeSameAsAllLevel::SAME_AS_ALL = ShouldBeSameAsAllLevel();

class CustomLogLevelUnitTest : public ::testing::Test {

    protected:

        CustomLogLevelUnitTest() {}

        ~CustomLogLevelUnitTest() {}

        void SetUp() override {

        }

        void TearDown() override {

        }

};

TEST_F(CustomLogLevelUnitTest, ShouldBeLessThanAll) {
    LogLevel logLevel = CustomLogLevel::CUSTOM;
    stringstream stringStream;
    string output;

    stringStream << logLevel;
    stringStream >> output;

    ASSERT_LT(logLevel, LogLevel::ALL);
    ASSERT_EQ(logLevel.getLevel(), 1000);
    ASSERT_STREQ(logLevel.getLabel(), "CUSTOM_LEVEL");
    ASSERT_EQ(output, "CUSTOM_LEVEL");
}

TEST_F(CustomLogLevelUnitTest, ShouldNotAcceptNegativeValues) {
    LogLevel logLevel = ShouldBeSameAsAllLevel::SAME_AS_ALL;
    stringstream stringStream;
    string output;

    stringStream << logLevel;
    stringStream >> output;

    ASSERT_EQ(logLevel, LogLevel::ALL);
    ASSERT_STREQ(logLevel.getLabel(), "ANOTHER_ALL");
    ASSERT_EQ(output, "ANOTHER_ALL");
}