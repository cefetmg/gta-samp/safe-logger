#include "gtest/gtest.h"
#include "safe-logger/log-level.hpp"
#include <limits.h>
#include <iostream>
#include <sstream>

using namespace std;
using namespace safelogger;

class LogLevelUnitTest : public ::testing::Test {

    protected:

        LogLevelUnitTest() {}

        ~LogLevelUnitTest() {}

        void SetUp() override {}

        void TearDown() override {}

};

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesOffLevel) {
    LogLevel level = LogLevel::OFF;

    ASSERT_EQ(level.getLevel(), 0);
    ASSERT_STREQ(level.getLabel(), "");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesFatalLevel) {
    LogLevel level = LogLevel::FATAL;

    ASSERT_EQ(level.getLevel(), 1);
    ASSERT_STREQ(level.getLabel(), "FATAL");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesErrorLevel) {
    LogLevel level = LogLevel::ERROR;

    ASSERT_EQ(level.getLevel(), 2);
    ASSERT_STREQ(level.getLabel(), "ERROR");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesWarnLevel) {
    LogLevel level = LogLevel::WARN;

    ASSERT_EQ(level.getLevel(), 3);
    ASSERT_STREQ(level.getLabel(), "WARN");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesInfoLevel) {
    LogLevel level = LogLevel::INFO;

    ASSERT_EQ(level.getLevel(), 4);
    ASSERT_STREQ(level.getLabel(), "INFO");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesDebugLevel) {
    LogLevel level = LogLevel::DEBUG;

    ASSERT_EQ(level.getLevel(), 5);
    ASSERT_STREQ(level.getLabel(), "DEBUG");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesTraceLevel) {
    LogLevel level = LogLevel::TRACE;

    ASSERT_EQ(level.getLevel(), 6);
    ASSERT_STREQ(level.getLabel(), "TRACE");
}

TEST_F(LogLevelUnitTest, ShouldMatchPrimitiveValuesAllLevel) {
    LogLevel level = LogLevel::ALL;

    ASSERT_EQ(level.getLevel(), UINT_MAX);
    ASSERT_STREQ(level.getLabel(), "ALL");
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnFatalLevel) {
    LogLevel level = LogLevel::FATAL;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_EQ(level, LogLevel::FATAL);
    ASSERT_LT(level, LogLevel::ERROR);
    ASSERT_LT(level, LogLevel::WARN);
    ASSERT_LT(level, LogLevel::INFO);
    ASSERT_LT(level, LogLevel::DEBUG);
    ASSERT_LT(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnErrorLevel) {
    LogLevel level = LogLevel::ERROR;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_EQ(level, LogLevel::ERROR);
    ASSERT_LT(level, LogLevel::WARN);
    ASSERT_LT(level, LogLevel::INFO);
    ASSERT_LT(level, LogLevel::DEBUG);
    ASSERT_LT(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnWarnLevel) {
    LogLevel level = LogLevel::WARN;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_GT(level, LogLevel::ERROR);
    ASSERT_EQ(level, LogLevel::WARN);
    ASSERT_LT(level, LogLevel::INFO);
    ASSERT_LT(level, LogLevel::DEBUG);
    ASSERT_LT(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnInfoLevel) {
    LogLevel level = LogLevel::INFO;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_GT(level, LogLevel::ERROR);
    ASSERT_GT(level, LogLevel::WARN);
    ASSERT_EQ(level, LogLevel::INFO);
    ASSERT_LT(level, LogLevel::DEBUG);
    ASSERT_LT(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnDebugLevel) {
    LogLevel level = LogLevel::DEBUG;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_GT(level, LogLevel::ERROR);
    ASSERT_GT(level, LogLevel::WARN);
    ASSERT_GT(level, LogLevel::INFO);
    ASSERT_EQ(level, LogLevel::DEBUG);
    ASSERT_LT(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnTraceLevel) {
    LogLevel level = LogLevel::TRACE;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_GT(level, LogLevel::ERROR);
    ASSERT_GT(level, LogLevel::WARN);
    ASSERT_GT(level, LogLevel::INFO);
    ASSERT_GT(level, LogLevel::DEBUG);
    ASSERT_EQ(level, LogLevel::TRACE);
    ASSERT_LT(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOrderPrioritiesWithNonEqualOperatorsOnAllLevel) {
    LogLevel level = LogLevel::ALL;

    ASSERT_GT(level, LogLevel::OFF);
    ASSERT_GT(level, LogLevel::FATAL);
    ASSERT_GT(level, LogLevel::ERROR);
    ASSERT_GT(level, LogLevel::WARN);
    ASSERT_GT(level, LogLevel::INFO);
    ASSERT_GT(level, LogLevel::DEBUG);
    ASSERT_GT(level, LogLevel::TRACE);
    ASSERT_EQ(level, LogLevel::ALL);
}

TEST_F(LogLevelUnitTest, ShouldOutputOffLabelToOutputStream) {
    LogLevel level = LogLevel::OFF;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output.size(), 0);
    ASSERT_EQ(output, "");
}

TEST_F(LogLevelUnitTest, ShouldOutputFatalLabelToOutputStream) {
    LogLevel level = LogLevel::FATAL;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "FATAL");
}

TEST_F(LogLevelUnitTest, ShouldOutputErrorLabelToOutputStream) {
    LogLevel level = LogLevel::ERROR;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "ERROR");
}

TEST_F(LogLevelUnitTest, ShouldOutputWarnLabelToOutputStream) {
    LogLevel level = LogLevel::WARN;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "WARN");
}

TEST_F(LogLevelUnitTest, ShouldOutputInfoLabelToOutputStream) {
    LogLevel level = LogLevel::INFO;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "INFO");
}

TEST_F(LogLevelUnitTest, ShouldOutputDebugLabelToOutputStream) {
    LogLevel level = LogLevel::DEBUG;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "DEBUG");
}

TEST_F(LogLevelUnitTest, ShouldOutputTraceLabelToOutputStream) {
    LogLevel level = LogLevel::TRACE;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "TRACE");
}

TEST_F(LogLevelUnitTest, ShouldOutputAllLabelToOutputStream) {
    LogLevel level = LogLevel::ALL;
    stringstream stringStream;
    string output;

    stringStream << level;
    
    stringStream >> output;
    ASSERT_EQ(output, "ALL");
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsOffLevel) {
    LogLevel logLevel = LogLevel::OFF;

    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_LE(logLevel, LogLevel::OFF);
    ASSERT_LE(logLevel, LogLevel::FATAL);
    ASSERT_LE(logLevel, LogLevel::ERROR);
    ASSERT_LE(logLevel, LogLevel::WARN);
    ASSERT_LE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsFatalLevel) {
    LogLevel logLevel = LogLevel::FATAL;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_LE(logLevel, LogLevel::FATAL);
    ASSERT_LE(logLevel, LogLevel::ERROR);
    ASSERT_LE(logLevel, LogLevel::WARN);
    ASSERT_LE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsErrorLevel) {
    LogLevel logLevel = LogLevel::ERROR;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_GE(logLevel, LogLevel::ERROR);
    ASSERT_LE(logLevel, LogLevel::ERROR);
    ASSERT_LE(logLevel, LogLevel::WARN);
    ASSERT_LE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsWarnLevel) {
    LogLevel logLevel = LogLevel::WARN;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_GE(logLevel, LogLevel::ERROR);
    ASSERT_GE(logLevel, LogLevel::WARN);
    ASSERT_LE(logLevel, LogLevel::WARN);
    ASSERT_LE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsInfoLevel) {
    LogLevel logLevel = LogLevel::INFO;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_GE(logLevel, LogLevel::ERROR);
    ASSERT_GE(logLevel, LogLevel::WARN);
    ASSERT_GE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::INFO);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsDebugLevel) {
    LogLevel logLevel = LogLevel::DEBUG;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::TRACE);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_GE(logLevel, LogLevel::ERROR);
    ASSERT_GE(logLevel, LogLevel::WARN);
    ASSERT_GE(logLevel, LogLevel::INFO);
    ASSERT_GE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::DEBUG);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}

TEST_F(LogLevelUnitTest, ShouldMatchUnequalOperatorsTraceLevel) {
    LogLevel logLevel = LogLevel::TRACE;

    ASSERT_NE(logLevel, LogLevel::OFF);
    ASSERT_NE(logLevel, LogLevel::FATAL);
    ASSERT_NE(logLevel, LogLevel::ERROR);
    ASSERT_NE(logLevel, LogLevel::WARN);
    ASSERT_NE(logLevel, LogLevel::INFO);
    ASSERT_NE(logLevel, LogLevel::DEBUG);
    ASSERT_GE(logLevel, LogLevel::OFF);
    ASSERT_GE(logLevel, LogLevel::FATAL);
    ASSERT_GE(logLevel, LogLevel::ERROR);
    ASSERT_GE(logLevel, LogLevel::WARN);
    ASSERT_GE(logLevel, LogLevel::INFO);
    ASSERT_GE(logLevel, LogLevel::DEBUG);
    ASSERT_GE(logLevel, LogLevel::TRACE);
    ASSERT_LE(logLevel, LogLevel::TRACE);
}