#ifndef SAFE_LOGGER_MOCK_FORMATTER_PIPE_COMMAND_H
#define SAFE_LOGGER_MOCK_FORMATTER_PIPE_COMMAND_H

#include "gmock/gmock.h"
#include "safe-logger/formatter/pipes/formatter-pipe-command.hpp"
#include <string>

namespace safelogger {

class MockFormatterPipeCommand : public FormatterPipeCommand {

    public:

        MOCK_METHOD(const std::string, runFormat, (const std::string &t_messageFormat), (const, override));
};

}

#endif