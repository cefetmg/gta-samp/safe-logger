#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "safe-logger-console-appender/safe-logger-console-appender.hpp"

#include <sstream>
#include <iostream>

using namespace std;
using namespace safelogger;

class SafeLoggerConsoleAppenderUnitTest : public ::testing::Test {

    private:

        streambuf *coutStreamBuf;
        
        streambuf *cerrStreamBuf;

    protected:

        ostringstream sOutStream;

        ostringstream sErrStream;

        SafeLoggerConsoleAppender consoleAppender;

    protected:

        SafeLoggerConsoleAppenderUnitTest() {}

        ~SafeLoggerConsoleAppenderUnitTest() {}

        void SetUp() override {
            coutStreamBuf = cout.rdbuf();
            cout.rdbuf(sOutStream.rdbuf());

            cerrStreamBuf = cerr.rdbuf();
            cerr.rdbuf(sErrStream.rdbuf());
        }

        void TearDown() override {
            cout.rdbuf(coutStreamBuf);
            cerr.rdbuf(cerrStreamBuf);
        }

};

TEST_F(SafeLoggerConsoleAppenderUnitTest, ShouldAppendMessageToStandardOutput) {
    consoleAppender.appendStandardMessage("This is a simple and default standard message.");

    ASSERT_EQ("This is a simple and default standard message.\n", sOutStream.str());
}

TEST_F(SafeLoggerConsoleAppenderUnitTest, ShouldAppendMessageToErrorOutput) {
    consoleAppender.appendErrorMessage("This is a simple and default error message.");

    ASSERT_EQ("This is a simple and default error message.\n", sErrStream.str());
}