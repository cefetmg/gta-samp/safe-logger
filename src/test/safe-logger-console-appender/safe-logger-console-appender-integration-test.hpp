#include "gtest/gtest.h"
#include <dlfcn.h>
#include "safe-logger-appender/appender.hpp"
#include "safe-logger-appender/appender-factory.hpp"
#include <sstream>

using namespace std;
using namespace safelogger;

class SafeLoggerConsoleAppenderIntegrationTest : public ::testing::Test {
    
    private:

        streambuf *coutStreamBuf;
        
        streambuf *cerrStreamBuf;

    protected:

        ostringstream sOutStream;

        ostringstream sErrStream;

    protected:

        std::string libName;

        SafeLoggerConsoleAppenderIntegrationTest() {
            libName = CURRENT_PATH + "/../lib/libsafe_logger_console_appender.so";
        }

        ~SafeLoggerConsoleAppenderIntegrationTest() {}

        void SetUp() override {
            coutStreamBuf = cout.rdbuf();
            cout.rdbuf(sOutStream.rdbuf());

            cerrStreamBuf = cerr.rdbuf();
            cerr.rdbuf(sErrStream.rdbuf());
        }

        void TearDown() override {
            cout.rdbuf(coutStreamBuf);
            cerr.rdbuf(cerrStreamBuf);
        }
};

TEST_F(SafeLoggerConsoleAppenderIntegrationTest, ShouldLoadLibraryAndFindSymbols) {
    void *libHandle = dlopen(libName.c_str(), RTLD_LAZY);
    
    ASSERT_NE(libHandle, nullptr);

    createAppender_t *createFactory = (createAppender_t *) dlsym(libHandle, "createAppender");

    ASSERT_NE(createFactory, nullptr);

    dlclose(libHandle);
}

TEST_F(SafeLoggerConsoleAppenderIntegrationTest, ShouldCreateSafeLoggerConsoleAppender) {
    void *libHandle = dlopen(libName.c_str(), RTLD_LAZY);    
    createAppender_t *createFactory = (createAppender_t *) dlsym(libHandle, "createAppender");

    safelogger::Appender *appender = createFactory();

    ASSERT_NE(appender, nullptr);
    delete appender;
    dlclose(libHandle);
}

TEST_F(SafeLoggerConsoleAppenderIntegrationTest, ShouldAppendStandardConsoleMessage) {
    void *libHandle = dlopen(libName.c_str(), RTLD_LAZY);    
    createAppender_t *createFactory = (createAppender_t *) dlsym(libHandle, "createAppender");
    Appender *appender = createFactory();

    appender->appendStandardMessage("This is just a test message");
    
    ASSERT_EQ(sOutStream.str(), "This is just a test message\n");
    ASSERT_NE(appender, nullptr);
    delete appender;
    dlclose(libHandle);
}

TEST_F(SafeLoggerConsoleAppenderIntegrationTest, ShouldAppendStandardErrorMessage) {
    void *libHandle = dlopen(libName.c_str(), RTLD_LAZY);    
    createAppender_t *createFactory = (createAppender_t *) dlsym(libHandle, "createAppender");
    Appender *appender = createFactory();

    appender->appendErrorMessage("This is just a test message");
    
    ASSERT_EQ(sErrStream.str(), "This is just a test message\n");
    ASSERT_NE(appender, nullptr);
    delete appender;
    dlclose(libHandle);
}