if(UNIX)

  list(APPEND COVERAGE_COMPILER_FLAGS "-fprofile-arcs" "-ftest-coverage")

  message(
    STATUS
      "Compiling from UNIX platform. Code coverage will be measured by gcov")

  list(APPEND GTEST_MAIN_DEPENDENCIES "gcov")
  list(APPEND GTEST_MOCK_DEPENDENCIES "gcov")

  add_compile_options(${COVERAGE_COMPILER_FLAGS})
  message(
    STATUS "Appending code coverage compiler flags: ${COVERAGE_COMPILER_FLAGS}")
else()
  message(WARNING "Cannot build with coverage on non-UNIX platform")
endif()
