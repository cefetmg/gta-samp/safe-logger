#include "safe-logger/log-level-unit-test.hpp"
#include "safe-logger/custom-log-level-unit-test.hpp"
#include "safe-logger/formatter/arguments-processor-unit-test.hpp"
#include "safe-logger/formatter/pipes/message-formatter-pipe-command-unit-test.hpp"
#include "safe-logger/formatter/pipes/datetime-formatter-pipe-command-unit-test.hpp"
#include "safe-logger/formatter/pipes/thread-id-formatter-pipe-command-unit-test.hpp"
#include "safe-logger/formatter/pipes/logger-formatter-pipe-command-unit-test.hpp"
#include "safe-logger/formatter/pipes/level-formatter-pipe-command-unit-test.hpp"
#include "safe-logger/formatter/formatter-pipeline-runner-unit-test.hpp"
#include "safe-logger/formatter/standard-formatter-pipeline-runner-facade-unit-test.hpp"
#include "safe-logger-console-appender/safe-logger-console-appender-unit-tests.hpp"
#include "gtest/gtest.h"

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}