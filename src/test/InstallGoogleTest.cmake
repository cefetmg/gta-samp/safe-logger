execute_process(
  COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/extern/googletest)
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(
  COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/extern/googletest)
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

set(gtest_force_shared_crt
    ON
    CACHE BOOL "" FORCE)

add_subdirectory(${CMAKE_SOURCE_DIR}/../extern/googletest
                 ${CMAKE_BINARY_DIR}/extern/build/googletest EXCLUDE_FROM_ALL)
