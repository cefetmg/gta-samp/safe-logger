#include "safe-logger-appender/appender-factory.hpp"
#include "safe-logger-appender/appender.hpp"
#include <dlfcn.h>
#include <assert.h>
#include <iostream>

const char *libName = "libsafe_logger_console_appender.so";

int shouldHaveSymbolsForCreateAndDestroyAndIncludePathAsWell() {
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldHaveSymbolsForCreateAndDestroyInLdPath]: Start" << std::endl;
    
    void *libHandle = dlopen(libName, RTLD_LAZY);
    
    assert( libHandle != nullptr );
    
    dlclose(libHandle);
    
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldHaveSymbolsForCreateAndDestroyInLdPath]: End" << std::endl;
    
    return 0;
}

int shouldAppendMessageToStandardOutput() {
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldAppendMessageToStandardOutput]: Start" << std::endl;
    
    void *libHandle = dlopen(libName, RTLD_LAZY);    
    assert( libHandle != nullptr );

    createAppender_t *factory = (createAppender_t *) dlsym(libHandle, "createAppender");

    assert(factory != nullptr);

    safelogger::Appender *appender = factory();

    assert(appender != nullptr);

    appender->appendStandardMessage("I am a simple and default message.");

    delete appender;
    
    dlclose(libHandle);
    
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldAppendMessageToStandardOutput]: End" << std::endl;
    
    return 0;
}

int shouldAppendMessageErrorOutput() {
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldAppendMessageErrorOutput]: Start" << std::endl;
    
    void *libHandle = dlopen(libName, RTLD_LAZY);    
    assert( libHandle != nullptr );

    createAppender_t *factory = (createAppender_t *) dlsym(libHandle, "createAppender");

    assert(factory != nullptr);

    safelogger::Appender *appender = factory();

    assert(appender != nullptr);

    appender->appendErrorMessage("I am a simple error message.");

    delete appender;
    
    dlclose(libHandle);
    
    std::cout << "[SafeLoggerConsoleAppenderTest/shouldAppendMessageErrorOutput]: End" << std::endl;
    
    return 0;
}

int RunAllSafeLoggerConsoleAppenderTests() {
    return shouldHaveSymbolsForCreateAndDestroyAndIncludePathAsWell() |
        shouldAppendMessageToStandardOutput() |
        shouldAppendMessageErrorOutput();
}