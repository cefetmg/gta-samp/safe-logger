#include "safe-logger/log-level.hpp"
#include <assert.h>
#include <limits.h>
#include <iostream>

class CustomLogLevel : public safelogger::LogLevel {

    private:
        CustomLogLevel() : safelogger::LogLevel(1000, "CUSTOM_LEVEL") {}

    public:
        static const safelogger::LogLevel CUSTOM;

};

const safelogger::LogLevel CustomLogLevel::CUSTOM = CustomLogLevel();

int shouldHaveAllLogLevelsRegistered() {
    std::cout << "[LogLevelTest/shouldHaveAllLogLevelsRegistered]: Start" << std::endl;
    assert(0 == safelogger::LogLevel::OFF.getLevel());
    assert(1 == safelogger::LogLevel::FATAL.getLevel());
    assert(2 == safelogger::LogLevel::ERROR.getLevel());
    assert(3 == safelogger::LogLevel::WARN.getLevel());
    assert(4 == safelogger::LogLevel::INFO.getLevel());
    assert(5 == safelogger::LogLevel::DEBUG.getLevel());
    assert(6 == safelogger::LogLevel::TRACE.getLevel());
    assert(UINT_MAX == safelogger::LogLevel::ALL.getLevel());
    std::cout << "[LogLevelTest/shouldHaveAllLogLevelsRegistered]: End" << std::endl;
    return 0;
}

int shouldPrintIoMessagesCorrectly() {
    std::cout << "[LogLevelTest/shouldPrintIoMessagesCorrectly]: Start" << std::endl;
    std::cout << "Testing if messages print correctly" << std::endl;
    assert( (std::cout << "Look! I am a precise " << safelogger::LogLevel::FATAL << " log level!" << std::endl) );
    std::cout << "[LogLevelTest/shouldPrintIoMessagesCorrectly]: End" << std::endl;
    return 0;
}

int shouldCompareToCustomLevel() {
    std::cout << "[LogLevelTest/shouldCompareToCustomLevel]: Start" << std::endl;
    std::cout << "Testing if custom log levels work" << std::endl;
    assert(CustomLogLevel::CUSTOM < safelogger::LogLevel::ALL);
    std::cout << "[LogLevelTest/shouldCompareToCustomLevel]: End" << std::endl;
    return 0;
}

int RunAllLogLevelTests() {
    return shouldHaveAllLogLevelsRegistered() |
        shouldPrintIoMessagesCorrectly() |
        shouldCompareToCustomLevel();
}