#include "safe-logger/formatter/standard-formatter-pipeline-runner-facade.hpp"
#include <assert.h>
#include <iostream>
#include <string>
#include <memory>

std::shared_ptr<safelogger::FormatterPipelineRunnerFacade> facade = 
    std::shared_ptr<safelogger::FormatterPipelineRunnerFacade>(new safelogger::StandardFormatterPipelineRunnerFacade());


int shouldOutputLogInRequiredFormat() {
    std::cout << "[StandardFormatterPipelineRunnerFacadeTest/shouldOutputLogInRequiredFormat]: Start" << std::endl;
    std::cout << "Testing log in required format" << std::endl;
    std::string result = facade->createFrom(
        "%Y-%m-%dT%T.{millis}%z", 
        safelogger::LogLevel::INFO, "PipelineRunnerTest",
        "A simple test to see if the {} was correctly packaged with {}/{} of the result package.",
        {"FormatterPipelineRunnerFacade", "2"},
        "[%datetime] %logger t%thread %level - %message\n");
    
    assert(std::cout << result);
    std::cout << "[StandardFormatterPipelineRunnerFacadeTest/shouldOutputLogInRequiredFormat]: End" << std::endl;
    return 0;
}

int RunAllPipelineRunnerTests() {
    int res = shouldOutputLogInRequiredFormat();

    return res;
}