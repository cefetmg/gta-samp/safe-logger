import os

from conans import ConanFile, CMake, tools


class SafeLoggerConanTest(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_BUILD_TYPE"] = "Debug"
        cmake.configure()
        cmake.build()

    def package_info(self):
        self.cpp_info.libs = ["safe_logger"]

    def test(self):
        os.chdir("bin")
        self.run(".%ssafe_logger_test_pkg" % os.sep)
