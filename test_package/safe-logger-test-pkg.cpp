#include <iostream>
#include <assert.h>
#include "src/log-level-test.hpp"
#include "src/standard-formatter-pipeline-runner-facade-test.hpp"
#include "src/safe-logger-console-appender-test.hpp"

int main(int argc, char *argv[]) {
    return RunAllLogLevelTests() | RunAllPipelineRunnerTests() | RunAllSafeLoggerConsoleAppenderTests();
}