from conans import ConanFile, CMake
from os import environ

def is_release_build():
    return environ.get('CI_COMMIT_TAG') is not None

def is_local_build():
    return environ.get('CI_COMMIT_TAG') is None and environ.get('CI_COMMIT_SHORT_SHA') is None

def get_version():
    if is_release_build():
        print('Building release version of the package', environ.get('CI_COMMIT_TAG'))
        return environ.get('CI_COMMIT_TAG')
    elif not is_local_build():
        print('Building develioment version of the package', environ.get('CI_COMMIT_SHORT_SHA'))
        return environ.get('CI_COMMIT_SHORT_SHA')
    print('Building local version of the package')
    return 'Local'

class SafeLoggerConan(ConanFile):
    name = "SafeLogger"
    user = "Cefetmg"
    channel = "Dev"
    license = "MIT"
    author = "Rodrigo Novaes rodrigo.novaes.jr@gmail.com"
    description = "A conan package with a logging API distribution"
    topics = ("Logger", "Logging", "SDK")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"
    exports_sources = "src/*"
    build_requires = []

    def set_version(self):
        self.version = get_version()

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_TESTING"] = 0
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        self.copy("*.hpp*", src="src/main/header", dst="include")
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.cppflags = ["-fopenmp"]
        self.cpp_info.libs = ["safe_logger"]
